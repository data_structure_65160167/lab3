import java.util.Scanner;

public class sqrt {
    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        int num = kb.nextInt();
        int result = Sqrt(num);
        System.out.print(result);
    }
    public static int Sqrt(int num){
        if (num < 2)
            return num;
        int left = 1;
        int right = num;
        int mid = 0;
        while (left <= right) {
            mid = (int)(left + Math.floor((right - left) / 2));
            if (mid > num / mid) {
                right = mid - 1;
            } else if ((mid + 1) > num / (mid + 1)) {
                return mid;
            } else {
                left = mid + 1;
            }
        }
        return mid;
    }
}